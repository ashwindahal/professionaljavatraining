package com.cerotid.java.ProfessionalJavaTraining;

//Primitive data types - includes byte, short, int, long, float, double, boolean and char, String 
public class Primative {

	public static void main(String[] args) {

		// Store integer values
		int number1 = 5;
		int number2 = 5;
		int sum = number1 + number2;

		// Store String characters
		String myText = "Java";

		// Mutable(We can change the value) Example:
		sum = 4; 
		System.out.println(sum);
		

	}

}
